# autowire-no-work
Example code for blog post on three reasons why an autowired dependency might be null

For more information see [Why is my @Autowired Component Null?](https://amydegregorio.com/2020/10/04/why-is-my-autowired-component-null/)

##### To Run:

```
> gradlew bootRun
```

#### License

This project is licensed under the Apache License version 2.0.  
See the LICENSE file or go to [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) for more information. 
