package com.amydegregorio.autowirenowork.common;

import org.springframework.stereotype.Service;

@Service
public class ColorConversionService {

   public String convertRgbToHex(int red, int blue, int green) {
      if (red < 0 || red > 255) {
         throw new IllegalArgumentException("Value for 'red' must be between 0 and 255.");
      }
      if (blue < 0 || blue > 255) {
         throw new IllegalArgumentException("Value for 'blue' must be between 0 and 255.");
      }
      if (green < 0 || green > 255) {
         throw new IllegalArgumentException("Value for 'green' must be between 0 and 255.");
      }
      return String.format("#%02x%02x%02x", red, blue, green);
   }
   
   public String convertHexToRgb(String hexCode) {
      if (hexCode == null) throw new NullPointerException("'hexCode' cannot be null.");
      if (hexCode.length() != 7) throw new IllegalArgumentException("Please provide a valid hexcode string in the format '#ffffff'");
      int i = Integer.decode(hexCode);
      int[] rgb = new int[]{(i >> 16) & 0xFF, (i >> 8) & 0xFF, i & 0xFF};
      return "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] +")";
   }
}
