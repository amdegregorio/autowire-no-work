package com.amydegregorio.autowirenowork.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amydegregorio.autowirenowork.common.ColorConversionService;

@RestController
@RequestMapping("/rest")
public class ColorConversionController {
   @Autowired
   private ColorConversionService colorConversionService;
   
   @GetMapping("/hex-to-rgb/{hex}")
   public String convertHexToRgb(@PathVariable("hex") String hex) {
      return colorConversionService.convertHexToRgb(hex);
   }
   
   @GetMapping("/rgb-to-hex/{red}/{blue}/{green}") 
   public String convertRgbToHex(@PathVariable("red") int red, @PathVariable("blue") int blue, @PathVariable("green") int green) {
      return colorConversionService.convertRgbToHex(red, blue, green);
   }
   
   @PreAuthorize("hasRole('ROLE_ADMIN')")
   @GetMapping("/admin/hex-to-rgb/{hex}")
   public String convertHexToRgbAdmin(@PathVariable("hex") String hex) {
      return colorConversionService.convertHexToRgb(hex) + " for admins";
   }
}
