package com.amydegregorio.autowirenowork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutowireNoWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutowireNoWorkApplication.class, args);
	}

}
