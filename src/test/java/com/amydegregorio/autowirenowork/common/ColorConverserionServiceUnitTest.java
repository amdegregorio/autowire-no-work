package com.amydegregorio.autowirenowork.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ColorConverserionServiceUnitTest {
   private Logger logger = LoggerFactory.getLogger(ColorConverserionServiceUnitTest.class);
   
   private final ColorConversionService conversionService = new ColorConversionService();

   @Test
   public void whenGivenRedBlueGreen_convertRgbToHex_returnsHexCode() {
      logger.info("Starting get 'whenGivenRedBlueGreen_convertRgbToHex_returnsHexCode'");
      String hexWhite = conversionService.convertRgbToHex(255, 255, 255);
      assertEquals("#ffffff", hexWhite);
   }
   
   @Test
   public void whenGivenIncorrectParameters_convertRgbToHex_throwsIllegalArgumentException() {
      logger.info("Starting get 'whenGivenIncorrectParameters_convertRgbToHex_throwsIllegalArgumentException'");
      assertThrows(IllegalArgumentException.class, () -> {
         conversionService.convertRgbToHex(-3, 255, 255);
         conversionService.convertRgbToHex(255, 300, 255);
         conversionService.convertRgbToHex(255, 255, 256);
      });
   }
   
   @Test
   public void whenGivenCorrectHexCode_convertHexToRgb_returnsRgbString() {
      logger.info("Starting get 'whenGivenCorrectHexCode_convertHexToRgb_returnsRgbString'");
      assertEquals("rgb(255,255,255)", conversionService.convertHexToRgb("#ffffff"));
   }
   
   @Test
   public void whenGivenNullParameters_convertHexToRgb_throwsNullPointerException() {
      logger.info("Starting get 'whenGivenNullParameters_convertHexToRgb_throwsNullPointerException'");
      assertThrows(NullPointerException.class, () -> {
         conversionService.convertHexToRgb(null);
      });
   }
   
   @Test
   public void whenGivenNullParameters_convertHexToRgb_throwsIllegalArgumentException() {
      logger.info("Starting get 'whenGivenNullParameters_convertHexToRgb_throwsIllegalArgumentException'");
      assertThrows(IllegalArgumentException.class, () -> {
         conversionService.convertHexToRgb("#000");
      });
   }
}
